object FrmFullView: TFrmFullView
  Left = 241
  Top = 134
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'FullView'
  ClientHeight = 446
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = menuContent
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyUp = FormKeyUp
  OnMouseDown = FormMouseDown
  OnMouseMove = FormMouseMove
  OnMouseUp = FormMouseUp
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  OnPaint = FormPaint
  PixelsPerInch = 96
  TextHeight = 13
  object menuContent: TPopupMenu
    Left = 16
    Top = 48
    object menuFirst: TMenuItem
      Caption = '&First'
      OnClick = menuFirstClick
    end
    object menuPrev: TMenuItem
      Caption = '&Prev'
      OnClick = menuPrevClick
    end
    object menuNext: TMenuItem
      Caption = '&Next'
      OnClick = menuNextClick
    end
    object menuLast: TMenuItem
      Caption = '&Last'
      OnClick = menuLastClick
    end
  end
end

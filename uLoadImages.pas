{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      29-mai-2014													}
{       Version    2.0                                  }
{       ����� ������� �������� ���������� � ������			}
{				��� ������������� ����������� ������ ���� � ����}
{       ������ TImageSyncList.
{       Created by Project ThumbsView                   }
{*******************************************************}
unit uLoadImages;

interface
uses System.Classes, System.SysUtils, Windows, Vcl.Graphics,
		ThreadBase, uImageList, ConstThumbs;

type
  EReadFile = class(Exception);
  RRect = record
		Width: Integer;
    Height: Integer;
  end;

  TLoadImages = class(TCustomThread)
  private
    FShowLog: Boolean;
    FHandle: THandle;
    FDirectoryName: string;
    FExitEvent: Boolean;
    procedure SendProgress(Value: integer; Tyyp: TProgressType);
    procedure GetImageFiles(DirName: string; Exts: array of string);
    procedure OpenImageDirectory;
    procedure ResizeImages;
    function GetSizedImageForThumb(FileName: String; var Rect: RRect): TBitmap;
  protected
    procedure InternalExec(Sender: TObject); override;
    procedure InternalException(const Status: Cardinal); override;

    procedure StartProgress;
    procedure Progress(Position: integer);
    procedure EndProgress;
  public
    constructor	Create(Handle: THandle);  reintroduce;
    destructor Destroy; override;
    procedure StartLoad(DirName: string);
    procedure ShowMessage(Value: String); override;
    property  ShowLog: Boolean read fShowLog write fShowLog;
  end;

var
  LoadImages: TLoadImages;

implementation
uses Winapi.GdipApi, Winapi.GdipObj, Winapi.GdipUtil, uLoger;

{ TLoadImages }
constructor TLoadImages.Create(Handle: THandle);
begin
  inherited Create;
  FHandle := Handle;
  ShowMessage('Create LoadImages thread ok');
end;

destructor TLoadImages.Destroy;
begin
  inherited;
  ShowMessage('Destroy LoadImages thread ok');
end;

procedure TLoadImages.SendProgress(Value: integer; Tyyp: TProgressType);
var
    pRecInfo: PInfo;
begin
  if FHandle <> 0 then
  begin
    New(pRecInfo);
    pRecInfo.Tyyp := Tyyp;
    pRecInfo.Value := Value;
  	PostMessage(FHandle, PWM_PROGRESS, 0, Integer(pRecInfo));
  end;
end;

procedure TLoadImages.StartProgress;
begin
  SendProgress(ImageSyncList.Count-1, prStart);
end;

procedure TLoadImages.Progress(Position: integer);
Const
			LastTime: Cardinal = 0;
begin
  if (GetTickCount - LastTime > 70) then
  begin
	  SendProgress(Position, prProgress);
    LastTime := GetTickCount;
  end;
end;

procedure TLoadImages.EndProgress;
begin
  SendProgress(0, prEnd);
end;

procedure TLoadImages.ShowMessage(Value: String);
begin
	TLoger.Show(Value);
end;

procedure TLoadImages.OpenImageDirectory;
begin
	if DirectoryExists(FDirectoryName) then
  begin
	  GetImageFiles(FDirectoryName, ['.jpg', '.bmp', '.gif', '.png']);
   	if FExitEvent then
			Exit;
  	ImageSyncList.Sort;
  end;
end;

procedure TLoadImages.GetImageFiles(DirName: string; Exts: array of string);
var
    SearchRec: TSearchRec;
    Ext: string;
    Item: PImageItem;
begin
  if DirectoryExists(DirName) then
  begin
    if FindFirst(DirName + '*.*', faAnyFile, SearchRec) = 0 then
    	if not (((SearchRec.Attr and (faHidden + faDirectory)) = 0)
      	and (SearchRec.Name[1] <> '.')) then
        repeat
          for Ext in Exts do
            if (Ext = LowerCase(ExtractFileExt(searchRec.Name))) then
            begin
              Item := ImageSyncList.GetZeroData;
              try
                with Item^ do
                begin
                  ImgName := searchRec.Name;
                  Size := searchRec.Size;
                  Image := nil;
                end;
                ImageSyncList.Add(Item);
              finally
                FreeMem(Item);
              end;
            end;
            if FExitEvent then
            	Exit;
        until (FindNext(SearchRec) <> 0);
    System.SysUtils.FindClose(SearchRec);
    ShowMessage(Format('Loaded %d image files.', [ImageSyncList.Count]));
  end;
end;

procedure TLoadImages.ResizeImages;
var
    i: Integer;
    Size: Integer;
    FileName: String;
    Bitmap: TBitmap;
		Rect: RRect;
    Item: PImageItem;
begin
	Size := ImageSyncList.Count - 1;
  for i := 0 to Size do
  begin
    ImageSyncList.Lock;
    try
    	Item := ImageSyncList[i];
    finally
      ImageSyncList.UnLock;
    end;

    if not Assigned(Item.Image) then
    begin
      FileName := FDirectoryName + ImageSyncList[i].ImgName;
      Bitmap := GetSizedImageForThumb(FileName, Rect);
      ImageSyncList.LockW;
      try
        Item.Width := Rect.Width;
        Item.Height := Rect.Height;
        Item.Image := Bitmap;
      finally
        ImageSyncList.UnLockW;
      end;
      Progress(i);
      if FExitEvent then
        Exit;
    end;

  end;
  EndProgress;
end;


function TLoadImages.GetSizedImageForThumb(FileName: String; var Rect: RRect):
	TBitmap;
var
    ImgSrc: TGPImage;
    ImgThumb: TGPImage;
    Graph: TGPGraphics;

    DrawWidth, DrawHeight: Integer;
    WValue, HValue, Rate: Double;
    RectDest: TGPRect;

    Status: TStatus;
begin
		ImgSrc := TGPImage.Create(FileName);
    try
      Rect.Width := ImgSrc.GetWidth;
      Rect.Height := ImgSrc.GetHeight;

      WValue := 1.0 * Rect.Width / ThumbWidth;
      HValue := 1.0 * Rect.Height / ThumbHeight;

      Rate := WValue;
      if HValue > WValue then
          Rate := HValue;

      if Rate > 1.0 then
      begin
          WValue := imgSrc.GetWidth / Rate;
          DrawWidth := Trunc(WValue);
          HValue := imgSrc.GetHeight / Rate;
          DrawHeight := Trunc(HValue);
      end
      else
      begin
          DrawWidth := Rect.Width;
          DrawHeight := Rect.Height;
      end;

      Result := TBitmap.Create;
      Result.Canvas.Lock;
      try
        Result.Transparent := true;
        Result.TransparentColor := clWhite;
        Result.Canvas.Brush.Color := clWhite;
        Result.PixelFormat := pf24bit;
        Result.SetSize(ThumbWidth, ThumbHeight);

        // rect
        RectDest.X := (ThumbWidth - DrawWidth + 2) shr 1;
        RectDest.Y := (ThumbHeight - DrawHeight + 2) shr 1;
        RectDest.Width := DrawWidth - 2;
        RectDest.Height := DrawHeight - 2;

        // gdiplus graw
        Graph := TGPGraphics.Create(Result.Canvas.Handle);
        try
          ImgThumb := ImgSrc.GetThumbnailImage(DrawWidth, DrawHeight);
          if (ImgThumb.GetLastStatus <> Ok) then
            ShowMessage('Error GetThumbnailImage status:' + IntToStr(Ord(ImgThumb.GetLastStatus)) );
          try
            Status := Graph.DrawImage(ImgThumb, RectDest, 0, 0, DrawWidth, DrawHeight, UnitPixel);
            if Status <> Ok then
              ShowMessage('Error DrawImage status:' + IntToStr(Ord(OK)) );
          finally
            ImgThumb.Free;
          end;
        finally
          Graph.Free;
        end;
      finally
      	Result.Canvas.UnLock;
      end;
    finally
	    ImgSrc.Free;
    end;
end;

procedure TLoadImages.InternalException(const Status: Cardinal);
begin
	if Terminated then
    exit;
  inherited;
  ShowMessage(fLastException.Message);
end;

procedure TLoadImages.StartLoad(DirName: string);
begin
  FExitEvent := True;
  WaitExecute;
  ImageSyncList.Clear;
  FExitEvent := False;
	FDirectoryName := DirName + '\';
  Start;
end;

procedure TLoadImages.InternalExec(Sender: TObject);
begin
	ShowMessage('Start loading image files');
  OpenImageDirectory;
 	if FExitEvent then
		Exit;
  StartProgress;
  ShowMessage('Start resize images');
  ResizeImages;
  EndProgress;
  inherited;
end;

end.

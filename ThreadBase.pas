{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      07-mai-2004							}
{       Version    2.0                                  }
{       Created by Project ACL Checker                  }
{*******************************************************}
unit ThreadBase;

interface
uses  Classes, Windows, sysutils, syncobjs;

type
  PRaiseFrame = ^TRaiseFrame;
  TRaiseFrame = record
    NextRaise: PRaiseFrame;
    ExceptAddr: Pointer;
    ExceptObject: TObject;
    ExceptionRecord: PExceptionRecord;
  end;


	{$I ActiveEvent.inc}
  RAction = record
    Event: array[TActiveEvent] of THandle;
    Call: array[TActiveEvent] of TNotifyEvent;
  end;


  TCustomThread = class(TThread, IUnknown)
  private
    fTimeOut: Longword;
    fAfterExecute: TNotifyEvent;
    fCall: Integer;
    fsData: TObject;
    function  GetID: Integer;
    function  GetDescription: String;
    function	GetExec: Integer;
  protected
    fExceptExit: Boolean;
    fRefCount: Integer;
    fID: Integer;
    fDescription: String;
    tmEvent: RAction;
    fLastException: Exception;
		fMetodExec: THandle;
    fManager: TCustomThread;
    procedure WaitTimeOut; virtual;
    procedure InternalExec(Sender: TObject); virtual;
    procedure InternalException(const Status: Cardinal); virtual;
    procedure InternalBreak; virtual;
    procedure BreakThread(Sender: TObject); virtual;
    function  SupportSession: Boolean;

    procedure Execute; override;
    procedure CreateEvent; virtual;
    procedure FreeEvent;   virtual;
    procedure DoAfterExecute;  virtual;
    procedure SetEvent(Value: TActiveEvent);
    procedure	SetTimeOut(Value: Longword);

    { IUnknown }
    function QueryInterface(const IID: TGUID; out Obj): HResult; virtual; stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
  public
  	constructor	Create(TimeOut: Longword = INFINITE; AOwner: TCustomThread = nil); virtual;
    destructor Destroy; override;
    procedure Start(const Data: TObject = nil); virtual;
    procedure Stop; virtual;
    procedure	WaitExecute;
    function	GetTerminated: Boolean; virtual;
    function  Active: Boolean; virtual;
    procedure ShowMessage(Value: String); virtual; abstract;

    property	Data: TObject read fsData;
    property  ID: Integer read GetID write fID;
    property  Description: String read GetDescription;
    property	Exec: Integer read GetExec;
    property  ExceptExit: Boolean read fExceptExit write fExceptExit;
    property  OnAfterExecute: TNotifyEvent read fAfterExecute write fAfterExecute;
  end;


procedure ThreadExit(Thread: TCustomThread);
procedure SleepRound(Delay: Cardinal; Reset: Boolean = False);

implementation

procedure SleepRound(Delay: Cardinal; Reset: Boolean = False);
Const
		NextRound: Cardinal = 0;
begin
  if Reset then
  	NextRound := 0;
	NextRound := NextRound + Delay;
	Sleep(NextRound - GetTickCount);
end;

procedure MsgWaitFor(Handle: THandle);
Var
    Msg: TMsg;
begin
  repeat
    case MsgWaitForMultipleObjects(1, Handle, False, INFINITE, QS_ALLINPUT) of
    	WAIT_OBJECT_0, WAIT_ABANDONED_0, $FFFFFFFF: Break;
    	WAIT_OBJECT_0 + 1:
        begin
          PeekMessage(Msg, 0, 0, 0, PM_Remove);
          if (Msg.message = 0) then
          	Break;
          TranslateMessage(Msg);
          DispatchMessage(Msg)
        end;
    end;
  until False;
end;

procedure ThreadExit(Thread: TCustomThread);
begin
  if Assigned(Thread) then
  begin
    Thread.Stop;
    Thread.Free;
  end;
end;

{ TCustomThread.IUnknown }

function TCustomThread.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then Result := S_OK
  else Result := E_NOINTERFACE
end;

function TCustomThread._AddRef: Integer;
begin
  Result := InterlockedIncrement(fRefCount);
end;

function TCustomThread._Release: Integer;
begin
  Result := InterlockedDecrement(fRefCount);
end;

{ TCustomThread }
constructor TCustomThread.Create(TimeOut: Longword = INFINITE; AOwner: TCustomThread = nil);
begin
  inherited Create(True);
  fManager := AOwner;
  fTimeOut := TimeOut;

  fID := Random(MaxInt-1000)+1000;

  tmEvent.Call[actSend] := InternalExec;
  tmEvent.Call[actExit] := BreakThread;

  fMetodExec := Windows.CreateEvent(nil, True, True, nil);
  CreateEvent;
  Resume;
end;

destructor TCustomThread.Destroy;
begin
  FreeEvent;
  if Assigned(fLastException) then
    fLastException.Free;
  CloseHandle(fMetodExec);
  inherited;
end;

procedure TCustomThread.BreakThread(Sender: TObject);
begin
  Terminate;
  if Assigned(fManager) then
    fManager.Start;
end;

procedure TCustomThread.InternalExec(Sender: TObject);
begin
  Inc(fCall);
end;

procedure TCustomThread.DoAfterExecute;
begin
  if Assigned(fAfterExecute) and not GetTerminated then
    fAfterExecute(fsData);
end;

procedure TCustomThread.InternalException(const Status: Cardinal);
begin
  if fLastException <> nil then
    fLastException.Free;

  if RaiseList <> nil then
  begin
    fLastException := Exception(PRaiseFrame(RaiseList)^.ExceptObject);
    PRaiseFrame(RaiseList)^.ExceptObject := nil;
  end
  else
    fLastException := nil;
end;

procedure TCustomThread.Execute;
var
    WaitState: DWord;
begin
  WaitState := 0;
	repeat
    try
      try
        WaitState := WaitForMultipleObjects(Ord(High(tmEvent.Event)) + 1, @tmEvent.Event,
          false, fTimeOut); //INFINITE

        if TActiveEvent(WaitState) <> actExit then
          Windows.ResetEvent(fMetodExec);

        case WaitState of
          Ord(Low(TActiveEvent))..Ord(High(TActiveEvent)):
          begin
            tmEvent.Call[TActiveEvent(WaitState)](Self);
            if (TActiveEvent(WaitState) <> actExit) then
            begin
              Windows.SetEvent(fMetodExec);
              DoAfterExecute;
            end;
          end;
          WAIT_TIMEOUT: WaitTimeOut;
        else
          OutputDebugString(PChar('Error WaitFor, status:'+IntToStr(Ord(WaitState))));
        end;
      except
        InternalException(WaitState);
        if fExceptExit then
          InternalBreak;
      end;
    finally
      Windows.SetEvent(fMetodExec);
    end;
  until Terminated;
end;

procedure TCustomThread.FreeEvent;
var
  i: TActiveEvent;
begin
  for i := Low(tmEvent.Event) to High(tmEvent.Event) do
  begin
    CloseHandle(tmEvent.Event[i]);
  end;
end;

procedure TCustomThread.CreateEvent;
var
  i: TActiveEvent;
begin
  for i := Low(tmEvent.Event) to High(tmEvent.Event) do
  begin
    tmEvent.Event[i] := Windows.CreateEvent(nil, false, false, nil);
  end;
end;

procedure TCustomThread.SetEvent(Value: TActiveEvent);
begin
  Windows.SetEvent(tmEvent.Event[Value]);
end;

procedure TCustomThread.Start(const Data: TObject = nil);
begin
 	fsData := Data;
  fCall := 0;
  Windows.ResetEvent(fMetodExec);
  SetEvent(actSend);
end;

procedure TCustomThread.Stop;
begin
  if Suspended then
    Resume;
  SetEvent(actExit);
  Terminate;
  MsgWaitFor(Handle);
end;

function TCustomThread.Active: Boolean;
begin
  Result := WaitForSingleObject(fMetodExec, 0) <> WAIT_OBJECT_0;
end;

function TCustomThread.GetDescription: String;
begin
  Result := fDescription;
end;

function TCustomThread.GetID: Integer;
begin
  Result := fID;
end;

function TCustomThread.GetExec: Integer;
begin
	Result := InterlockedExchangeAdd(@fCall, 0);
end;

procedure TCustomThread.WaitTimeOut;
begin
//
end;

procedure TCustomThread.WaitExecute;
begin
	WaitForSingleObject(fMetodExec, INFINITE);
end;

function TCustomThread.GetTerminated: Boolean;
begin
	Result := Terminated;
end;

procedure TCustomThread.SetTimeOut(Value: Longword);
begin
	fTimeOut := Value;
end;

procedure TCustomThread.InternalBreak;
begin
  BreakThread(nil);
end;

function TCustomThread.SupportSession: Boolean;
begin
  Result := False;
end;


end.

{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      27-mai-2014													}
{       Version    2.0                                  }
{       ����� ���������������� ������ ��� ��������			}
{       �����������                                     }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit uImageList;

interface

uses System.SysUtils, System.Classes, Vcl.Graphics, uCustomList, ConstThumbs;

type
    RImageItem = record
      ImgName: string;
      Image: TBitmap;
      Size: Int64;
      Width: Integer;
      Height: Integer;
      ImgIndex: Integer;
    end;
    PImageItem = ^RImageItem;

		TImageSyncList = class(TCustomList)
		protected
    	function	GetItem(Index: Integer): PImageItem;
      procedure	DeleteInternal(Index: LongWord; RemList: Boolean = True); override;
    public
      fCounter: Integer;
      constructor Create; override;

      procedure Add(Data: PImageItem); overload;
      property	Items[Index: Integer]: PImageItem read GetItem; default;
    end;

Var
		ImageSyncList: TImageSyncList;


implementation

function InternalCompare(Item1, Item2: Pointer): Integer;
begin
  Result := AnsiCompareStr(PImageItem(Item1).ImgName, PImageItem(Item2).ImgName);
end;

{ TImageSyncList }
constructor TImageSyncList.Create;
begin
  inherited;
  FSizeRec := SizeOf(RImageItem);
  FunctionCompare := InternalCompare;
end;

procedure TImageSyncList.DeleteInternal(Index: LongWord; RemList: Boolean);
var
		Item: PImageItem;
begin
	Item := GetItem(Index);
  if Assigned(Item.Image) then
  begin
  	Item.Image.Free;
    Item.Image := nil;
  end;
  inherited;
end;

procedure TImageSyncList.Add(Data: PImageItem);
var
		NewItem: PImageItem;
begin
  LockW;
  try
    NewItem := GetZeroData;
    System.Move(Data^, NewItem^, SizeOf(Data^));
    AddInternal(NewItem);
  finally
  	UnLockW;
  end;
end;

function TImageSyncList.GetItem(Index: Integer): PImageItem;
begin
  Result := PImageItem(fList[Index]);
end;


end.

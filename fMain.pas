{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      27-mai-2014													}
{       Version    2.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit fMain;

interface

uses
  System.Classes, System.SysUtils, Winapi.Messages, Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.Controls, Vcl.ExtCtrls, Vcl.Forms, Vcl.Graphics, Vcl.Dialogs,
  Vcl.Shell.ShellCtrls, Vcl.Menus, Vcl.XPMan, Vcl.ImgList,
  uLoger, ConstThumbs, uImageList;

type
  TFrmMain = class(TForm)
    pnlGrid: TPanel;
    pnlDebug: TPanel;
    pnlRight: TPanel;
    PnlLog: TPanel;
    pnlLogTitle: TPanel;
    mLog: TMemo;
    pnlProgress: TPanel;
    pbLoadFiles: TProgressBar;
    Panel4: TPanel;
    BtnCreateThead: TButton;
    CbShowLog: TCheckBox;
    StatusBar: TStatusBar;
    TvLocalDir: TShellTreeView;
    splMain: TSplitter;
    LvImgItems: TListView;
    MainMenu: TMainMenu;
    menuFile: TMenuItem;
    N2: TMenuItem;
    menuReloadDir: TMenuItem;
    N3: TMenuItem;
    menuExit: TMenuItem;
    menuAbout: TMenuItem;
    menuDebug: TMenuItem;
    ListView: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure CbShowLogClick(Sender: TObject);
    procedure Showdebuglog1Click(Sender: TObject);
    procedure TvLocalDirChange(Sender: TObject; Node: TTreeNode);
    procedure LvImgItemsData(Sender: TObject; Item: TListItem);
    procedure BtnCreateTheadClick(Sender: TObject);
    procedure LvImgItemsDataHint(Sender: TObject; StartIndex,
      EndIndex: Integer);
    procedure menuDebugClick(Sender: TObject);
    procedure TvLocalDirEditing(Sender: TObject; Node: TTreeNode;
      var AllowEdit: Boolean);
    procedure LvImgItemsResize(Sender: TObject);
    procedure LvImgItemsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure LvImgItemsDblClick(Sender: TObject);
    procedure menuReloadDirClick(Sender: TObject);
    procedure menuAboutClick(Sender: TObject);
    procedure menuExitClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    DirectoryName: string;
    procedure ThreadWndProc(var msg: TMessage );  message PWM_EVENT;
    procedure ThreadProgress(var msg: TMessage ); message PWM_PROGRESS;

    procedure CreateThread;
    procedure DestroyThread;
    procedure StartProgress(Value: Integer);
    procedure ShowProgress(Value: integer);
    procedure EndProgress;
    procedure	SetViewImage(Element: PImageItem);
    procedure StatusBarPanelText(Index: Integer; Text: String);
  public
    procedure HandleException(Sender: TObject; E: Exception);
  end;

var
  FrmMain: TFrmMain;

implementation
uses  ThreadBase, uLoadImages, fFullView;

{$R *.DFM}
procedure TFrmMain.ThreadWndProc(var msg: TMessage);
begin
  if mLog.Lines.Count > 32000 then
    mLog.Lines.Clear;
  mLog.Lines.Add(GetMsgStr(msg.LParam));
end;

procedure TFrmMain.TvLocalDirChange(Sender: TObject; Node: TTreeNode);
begin
  ListView.Clear;
  if Node.Level > 0 then
  begin
    DirectoryName := tvLocalDir.Path;
    LoadImages.StartLoad(tvLocalDir.Path);
  end;
end;

procedure TFrmMain.TvLocalDirEditing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
	AllowEdit := False;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin
  DoubleBuffered := True;
  Application.OnException := HandleException;
  CreateThread;
end;

procedure TFrmMain.FormDestroy(Sender: TObject);
begin
	DestroyThread;
end;

//�������� ���� ����������� ������� � ������� ��� ����������. ��� ��������
// ������ ��������� � ����� ������� � �������� ����������� ����������.
procedure TFrmMain.CreateThread;
begin
  TLoger.CreateEx(Handle);
  if not Assigned(ImageSyncList) then
    ImageSyncList := TImageSyncList.Create;
	LoadImages := TLoadImages.Create(Handle);
  ListView.Width := ThumbWidth;
  ListView.Height := ThumbHeight;
end;

//��������� � ������������ ������� � ������.
procedure TFrmMain.DestroyThread;
begin
  ThreadExit(LoadImages);
  LoadImages := nil;
  ImageSyncList.Free;
  ImageSyncList := nil;
  FreeAndNil(Loger);
end;

procedure TFrmMain.HandleException(Sender: TObject; E: Exception);
begin
  TLoger.Show(E.Message+' '+IntToStr(E.HelpContext));
end;

procedure TFrmMain.LvImgItemsData(Sender: TObject; Item: TListItem);
var
		Element: PImageItem;
begin
  if (Item.index < ImageSyncList.Count) and (Item.index >= 0) then
  begin
    ImageSyncList.Lock;
    try
	    Element := ImageSyncList.Items[Item.index];
  	  if Assigned(Element.Image) and (Element.ImgIndex = 0) then
      begin
	   		SetViewImage(Element);
      end;
    finally
			ImageSyncList.UnLock;
    end;

    Item.Caption := ExtractFileName(Element.ImgName);
    Item.ImageIndex := Element.ImgIndex;
  end;
end;

procedure TFrmMain.LvImgItemsDataHint(Sender: TObject; StartIndex,
  EndIndex: Integer);
var
	  i: Integer;
  	Element: PImageItem;
begin
  if (StartIndex < EndIndex) and (EndIndex <= ImageSyncList.Count) then
  begin
    for i := StartIndex to EndIndex do
    begin
	    ImageSyncList.Lock;
  	  try
        Element := ImageSyncList[i];
        if Assigned(Element.Image) and (Element.ImgIndex = 0) then
          SetViewImage(Element);
      finally
				ImageSyncList.UnLock;
      end;
    end;
  end;
end;

procedure TFrmMain.LvImgItemsDblClick(Sender: TObject);
begin
	if not Assigned(frmFullView) then
    Application.CreateForm(TFrmFullView, frmFullView);
  frmFullView.Show;
  frmFullView.SetShowImage(LvImgItems.Selected.Index, DirectoryName);
  frmFullView.Invalidate;
end;

procedure TFrmMain.SetViewImage(Element: PImageItem);
begin
  Element.ImgIndex := ListView.Add(Element.Image, nil);
end;

procedure TFrmMain.LvImgItemsResize(Sender: TObject);
begin
	TListView(Sender).Invalidate;
end;

procedure TFrmMain.LvImgItemsSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  	ViewItem: PImageItem;
begin
  if Selected then
  begin
    ViewItem := ImageSyncList[Item.Index];
    StatusBarPanelText(0, ViewItem.ImgName);
    StatusBarPanelText(1, IntToStr(ViewItem.Size) + ' Kb' );
		StatusBarPanelText(2, Format('Resolution %d X %d',
    	[ViewItem.Width, ViewItem.Height]));
  end;
end;

procedure TFrmMain.menuAboutClick(Sender: TObject);
begin
  ShowMessage('��� ��������� ���������� GDI+ ��� �������������� � ��������� ' +
  	'Thumbnail. � ��� ������������ ��� ����������� ������. ���� ������������' +
    ' �����������.');
end;

procedure TFrmMain.menuExitClick(Sender: TObject);
begin
	Close;
end;

procedure TFrmMain.menuReloadDirClick(Sender: TObject);
begin
  if (DirectoryName <> '') then
  begin
    ListView.Clear;
    LoadImages.StartLoad(DirectoryName);
  end;
end;

procedure TFrmMain.StatusBarPanelText(Index: Integer; Text: String);
var
    PanelWidth: Integer;
begin
	PanelWidth := StatusBar.Canvas.TextWidth(Text) + 30;
  StatusBar.Panels[Index].Width := PanelWidth;
  StatusBar.Panels[Index].Text := Text;
end;

procedure TFrmMain.menuDebugClick(Sender: TObject);
begin
	pnlDebug.Visible := True;
	LoadImages.ShowLog := True;
	CbShowLog.Checked := True;
end;

procedure TFrmMain.BtnCreateTheadClick(Sender: TObject);
begin
  CreateThread;
end;

procedure TFrmMain.ThreadProgress(var msg: TMessage);
var
    Info: PInfo;
begin
  Info := PInfo(msg.LParam);

  case Info.Tyyp of
    prStart:      StartProgress(Info.Value);
    prProgress:   ShowProgress(Info.Value);
    prEnd:        EndProgress;
  end;
  Dispose(Info);
end;



procedure TFrmMain.StartProgress(Value: Integer);
begin
  if (Value > 0) then
  begin
    pbLoadFiles.Max := Value;
    pnlProgress.Visible := True;
    lvImgItems.Items.Count := ImageSyncList.Count;
    Application.ProcessMessages;
  end;
end;

procedure TFrmMain.Showdebuglog1Click(Sender: TObject);
begin
	pnlDebug.Visible := True;
end;

procedure TFrmMain.ShowProgress(Value: integer);
begin
	pbLoadFiles.Position := Value;
  Application.ProcessMessages;
  LvImgItems.Invalidate;
end;

procedure TFrmMain.EndProgress;
begin
  pbLoadFiles.Max := pbLoadFiles.Position;
  pnlProgress.Visible := False;
  LvImgItems.Invalidate;
  Application.ProcessMessages;
end;

procedure TFrmMain.CbShowLogClick(Sender: TObject);
begin
  PnlLog.Visible := TCheckBox(Sender).Checked;
  LoadImages.ShowLog := TCheckBox(Sender).Checked;
end;

end.

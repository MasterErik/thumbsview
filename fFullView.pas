{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      30-mai-2014													}
{       Version    2.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit fFullView;

interface

uses
  System.Classes, System.SysUtils, System.Types, Winapi.Messages,
  Vcl.ComCtrls, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.Controls, Vcl.Forms, Vcl.Graphics, System.Variants,
  Vcl.Menus, Vcl.XPMan, Vcl.ImgList,
  GdipApi, GdipUtil, GdipObj;


type
    TFrmFullView = class(TForm)
    MenuContent: TPopupMenu;
    MenuFirst: TMenuItem;
    MenuPrev: TMenuItem;
    MenuNext: TMenuItem;
    MenuLast: TMenuItem;

    procedure FormCreate(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormPaint(Sender: TObject);
    procedure MenuFirstClick(Sender: TObject);
    procedure MenuLastClick(Sender: TObject);
    procedure MenuNextClick(Sender: TObject);
    procedure menuPrevClick(Sender: TObject);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure FormDestroy(Sender: TObject);

	private
    ImgBitmap: TBitmap;
    xSrcPos, ySrcPos, xMouse, yMouse: Integer;
    xOrigin, yOrigin, xOffset, yOffset: Integer;
    mgWidth, mgHeight: Integer;
    MouseDown: Boolean;
    RectSrc, RectDest: TRect;
    ImgIndex: Integer;
    DirectoryName: string;

    procedure SetImage(index: Integer);
    procedure ShowPrevImage;
    procedure ShowCurrentImage;
    procedure ShowNextImage;
    procedure SetShowRegion;
    procedure ShowImage;
	public
    procedure SetShowImage(Index: Integer; Dir: string);
	end;

var
    FrmFullView: TFrmFullView = nil;


implementation
uses uImageList;

{$R *.dfm}

procedure TFrmFullView.FormPaint(Sender: TObject);
begin
  if ImgIndex > -1 then
	  ShowImage;
end;

procedure TFrmFullView.SetImage(index: Integer);
var
    imgGdip: TGPImage;
    graph: TGPGraphics;
    rectDraw: TGPRect;
    FileName:  string;
begin
  if (index > -1) and (index < ImageSyncList.Count) then
  begin
    ImageSyncList.Lock;
    try
	    FileName := ImageSyncList.Items[index].ImgName;
    finally
	    ImageSyncList.UnLock;
    end;

    ImgIndex := index;

    if Assigned(ImgBitmap) then
    	ImgBitmap.Free;
    ImgBitmap := TBitmap.Create;

    imgGdip := TGPImage.Create(DirectoryName + '\' + FileName);
    ImgBitmap.Width := imgGdip.GetWidth;
    ImgBitmap.Height := imgGdip.GetHeight;
    graph := TGPGraphics.Create(ImgBitmap.Canvas.Handle);

    // rect
    rectDraw.X := 0;
    rectDraw.Y := 0;
    rectDraw.Width := imgGdip.GetWidth;
    rectDraw.Height := imgGdip.GetHeight;

    graph.DrawImage(imgGdip, rectDraw, 0, 0, imgGdip.GetWidth,
      imgGdip.GetHeight, UnitPixel);

    graph.Free;

    mgWidth := ImgBitmap.Width;
    mgHeight := ImgBitmap.Height;

    imgGdip.Free;
    repaint;
  end;
end;

procedure TFrmFullView.ShowCurrentImage;
begin
	SetImage(ImgIndex);
end;

procedure TFrmFullView.ShowNextImage;
begin
  if (ImgIndex < ImageSyncList.Count-1) then
		SetImage(ImgIndex + 1);
end;

procedure TFrmFullView.ShowPrevImage;
begin
  if (ImgIndex > 0) then
		SetImage(ImgIndex - 1);
end;

procedure TFrmFullView.SetShowRegion;
begin
  if ClientWidth > mgWidth then
  begin
    RectDest.Left := (ClientWidth - mgWidth) shr 1;
    RectDest.Right := mgWidth + RectDest.Left;
    RectSrc.Right := mgWidth;
  end
  else
  begin
    RectDest.Left := 0;
    RectSrc.Right := ClientWidth;
    RectDest.Right := ClientWidth;
  end;

  if ClientHeight > mgHeight then
  begin
    RectDest.Top := (ClientHeight - mgHeight) shr 1;
    RectDest.Bottom := mgHeight + RectDest.Top;
    RectSrc.Bottom := mgHeight;
  end
  else
  begin
    RectDest.Top := 0;
    RectDest.Bottom := ClientHeight;
    RectSrc.Bottom := ClientHeight;
  end;

  // srcpos
  if (xSrcPos < 0) or (ClientWidth > mgWidth) then
		xSrcPos := 0
  else if  xSrcPos + ClientWidth > mgWidth then
		xSrcPos := mgWidth - ClientWidth;

  if (ySrcPos < 0) or (ClientHeight > mgHeight) then
		ySrcPos := 0
  else if ySrcPos + ClientHeight > mgHeight then
		ySrcPos := mgHeight - ClientHeight;

  RectSrc.Left := xSrcPos;
  RectSrc.Right := RectSrc.Right + xSrcPos;
  RectSrc.Top := ySrcPos;
  RectSrc.Bottom := RectSrc.Bottom + ySrcPos;
end;

procedure TFrmFullView.ShowImage;
begin
  if Assigned(ImgBitmap) then
  begin
    SetShowRegion;
    Canvas.CopyRect(RectDest, ImgBitmap.Canvas, RectSrc);
  end;
end;

procedure TFrmFullView.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  MouseDown := true;
  if Button = mbLeft then
		Cursor := crHandPoint;
  xMouse := X;
  yMouse := Y;
  xOrigin := xSrcPos;
  yOrigin := ySrcPos;
end;

procedure TFrmFullView.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if MouseDown then
  begin
    xOffset := xMouse - X;
    yOffset := yMouse - Y;
    xSrcPos := xOrigin + xOffset;
    ySrcPos := yOrigin + yOffset;
    repaint;
  end;
end;

procedure TFrmFullView.FormMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  MouseDown := false;
  if Button = mbLeft then
		Cursor := crDefault;
  repaint;
end;


procedure TFrmFullView.FormCreate(Sender: TObject);
begin
  Color := clBlack;
  DoubleBuffered := true;
  ImgIndex := -1;
end;

procedure TFrmFullView.MenuFirstClick(Sender: TObject);
begin
  ImgIndex := 0;
  ShowCurrentImage;
end;

procedure TFrmFullView.menuPrevClick(Sender: TObject);
begin
  ShowPrevImage;
end;

procedure TFrmFullView.MenuNextClick(Sender: TObject);
begin
  ShowNextImage;
end;

procedure TFrmFullView.MenuLastClick(Sender: TObject);
begin
  ImgIndex := ImageSyncList.Count - 1;
  ShowCurrentImage;
end;

procedure TFrmFullView.FormKeyUp(Sender: TObject; var Key: Word; shift: TShiftState);
begin 
  Case Key of
    27: Visible := false;
    36, 38, 115: MenuFirst.Click;
    33, 37, 101: menuPrev.Click;
    34, 39, 100: MenuNext.Click;
    35, 40, 102: MenuLast.Click;
  end;
end;
 
procedure TFrmFullView.SetShowImage(Index: Integer; Dir: string);
begin
  DirectoryName := Dir;
  ImgIndex := Index;
  ShowCurrentImage;
end;

procedure TFrmFullView.FormMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  MenuNext.Click;
end;

procedure TFrmFullView.FormMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  MenuPrev.Click;
end;

procedure TFrmFullView.FormDestroy(Sender: TObject);
begin
  if Assigned(ImgBitmap) then
  begin
    ImgBitmap.Free;
    ImgBitmap := nil;
  end;
end;

end.

{*******************************************************}
{       Author:    Erik Ivanov                          }
{       Date:      07-mai-2007													}
{       Version    2.0                                  }
{       Created by Project ThumbsView                   }
{*******************************************************}
unit uCustomList;

interface

uses SysUtils, Classes;


type
  TCustomList = class
  private
    FLock: TMultiReadExclusiveWriteSynchronizer;
  protected
    FList: TList;
    FSizeRec: Integer;
    FunctionCompare: TListSortCompare;
    procedure AddInternal(P: Pointer);
    procedure	DeleteInternal(Index: LongWord; RemList: Boolean = True); virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    function  GetZeroData: Pointer;
    procedure	Sort;
    procedure Clear; virtual;
    procedure	Lock;
    procedure	UnLock;
    procedure LockW;
    procedure UnLockW;
    function	Count: Integer;
    function	CountInternal: Integer;
  end;

implementation


constructor TCustomList.Create;
begin
  inherited Create;
  fLock := TMultiReadExclusiveWriteSynchronizer.Create;
	fList := TList.Create;
end;

destructor TCustomList.Destroy;
begin
  Clear;
  fList.Free;
  FreeAndNil(fLock);
  inherited;
end;

function TCustomList.GetZeroData: Pointer;
begin
	GetMem(Result, FSizeRec);
  FillChar(Result^, FSizeRec, 0);
end;

procedure TCustomList.AddInternal(P: Pointer);
begin
  fList.Add(P);
end;

procedure TCustomList.DeleteInternal(Index: LongWord; RemList: Boolean = True);
begin
  FreeMem(fList.Items[Index]);
  if RemList then
  	fList.Delete(Index);
end;

procedure TCustomList.Sort;
begin
  LockW;
  try
		fList.Sort(FunctionCompare);
  finally
  	UnLockW;
  end;
end;

procedure TCustomList.Clear;
Var
		i: Integer;
begin
  LockW;
  try
    for i := Count-1 downto 0 do
      DeleteInternal(i, False);
		fList.Clear;
  finally
  	UnLockW;
  end;
end;

procedure TCustomList.Lock;
begin
	fLock.BeginRead;
end;

procedure TCustomList.UnLock;
begin
	fLock.EndRead;
end;

procedure TCustomList.LockW;
begin
	fLock.BeginWrite;
end;

procedure TCustomList.UnLockW;
begin
	fLock.EndWrite;
end;

function TCustomList.CountInternal: Integer;
begin
  Result := fList.Count;
end;

function TCustomList.Count: Integer;
begin
  Lock;
  try
    Result := fList.Count;
  finally
  	UnLock;
  end;

end;



end.

program ThumbsView;

uses
  Vcl.Forms,
  fMain in 'fMain.pas' {FrmMain},
  fFullView in 'fFullView.pas' {FrmFullView},
  ConstThumbs in 'ConstThumbs.pas',
  uLoger in 'uLoger.pas',
  ThreadBase in 'ThreadBase.pas',
  uLoadImages in 'uLoadImages.pas',
  uImageList in 'uImageList.pas',
  uCustomList in 'uCustomList.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.

object FrmMain: TFrmMain
  Left = 259
  Top = 121
  Caption = 'Main'
  ClientHeight = 622
  ClientWidth = 898
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnlGrid: TPanel
    Left = 0
    Top = 0
    Width = 898
    Height = 546
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object pnlRight: TPanel
      Left = 0
      Top = 0
      Width = 632
      Height = 546
      Align = alClient
      TabOrder = 0
      object splMain: TSplitter
        Left = 233
        Top = 1
        Height = 544
        ExplicitLeft = 201
        ExplicitTop = 36
        ExplicitHeight = 564
      end
      object TvLocalDir: TShellTreeView
        Left = 1
        Top = 1
        Width = 232
        Height = 544
        ObjectTypes = [otFolders]
        Root = 'rfDesktop'
        UseShellImages = True
        Align = alLeft
        AutoRefresh = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        Indent = 19
        ParentColor = False
        ParentFont = False
        ParentShowHint = False
        RightClickSelect = True
        ShowHint = False
        ShowLines = False
        ShowRoot = False
        TabOrder = 0
        OnChange = TvLocalDirChange
        OnEditing = TvLocalDirEditing
      end
      object LvImgItems: TListView
        Left = 236
        Top = 1
        Width = 395
        Height = 544
        Align = alClient
        Columns = <>
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial Narrow'
        Font.Style = []
        IconOptions.AutoArrange = True
        LargeImages = ListView
        OwnerData = True
        ReadOnly = True
        ParentFont = False
        ParentShowHint = False
        ShowHint = False
        TabOrder = 1
        OnData = LvImgItemsData
        OnDataHint = LvImgItemsDataHint
        OnDblClick = LvImgItemsDblClick
        OnResize = LvImgItemsResize
        OnSelectItem = LvImgItemsSelectItem
      end
    end
    object PnlLog: TPanel
      Left = 632
      Top = 0
      Width = 266
      Height = 546
      Align = alRight
      BevelInner = bvRaised
      TabOrder = 1
      Visible = False
      object pnlLogTitle: TPanel
        Left = 2
        Top = 2
        Width = 262
        Height = 26
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Logging'
        TabOrder = 0
      end
      object mLog: TMemo
        Left = 2
        Top = 28
        Width = 262
        Height = 516
        Align = alClient
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
  end
  object pnlDebug: TPanel
    Left = 0
    Top = 546
    Width = 898
    Height = 38
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object BtnCreateThead: TButton
      Left = 647
      Top = 6
      Width = 105
      Height = 25
      Caption = 'ReCreate Thread'
      TabOrder = 0
      OnClick = BtnCreateTheadClick
    end
    object CbShowLog: TCheckBox
      Left = 775
      Top = 10
      Width = 106
      Height = 17
      Caption = 'Show debug log'
      TabOrder = 1
      OnClick = CbShowLogClick
    end
  end
  object pnlProgress: TPanel
    Left = 0
    Top = 584
    Width = 898
    Height = 19
    Align = alBottom
    Alignment = taLeftJustify
    BevelInner = bvRaised
    BevelOuter = bvLowered
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    Visible = False
    object pbLoadFiles: TProgressBar
      Left = 113
      Top = 2
      Width = 783
      Height = 15
      Align = alClient
      TabOrder = 0
    end
    object Panel4: TPanel
      Left = 2
      Top = 2
      Width = 111
      Height = 15
      Align = alLeft
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'Load files progress:'
      TabOrder = 1
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 603
    Width = 898
    Height = 19
    Panels = <
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 200
      end>
  end
  object MainMenu: TMainMenu
    Left = 640
    Top = 8
    object menuFile: TMenuItem
      Caption = '&File'
      object N2: TMenuItem
        Caption = '-'
      end
      object menuReloadDir: TMenuItem
        Caption = '&ReloadDir'
        OnClick = menuReloadDirClick
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object menuExit: TMenuItem
        Caption = '&Exit'
        OnClick = menuExitClick
      end
    end
    object menuDebug: TMenuItem
      Caption = 'Debug'
      OnClick = menuDebugClick
    end
    object menuAbout: TMenuItem
      Caption = '&About'
      OnClick = menuAboutClick
    end
  end
  object ListView: TImageList
    Height = 160
    Width = 160
    Left = 560
    Top = 8
  end
end
